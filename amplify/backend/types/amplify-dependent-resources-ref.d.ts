export type AmplifyDependentResourcesAttributes = {
  "api": {
    "storeadmindashboard": {
      "GraphQLAPIEndpointOutput": "string",
      "GraphQLAPIIdOutput": "string",
      "GraphQLAPIKeyOutput": "string"
    }
  },
  "auth": {
    "storeadmindashboard": {
      "AppClientID": "string",
      "AppClientIDWeb": "string",
      "IdentityPoolId": "string",
      "IdentityPoolName": "string",
      "UserPoolArn": "string",
      "UserPoolId": "string",
      "UserPoolName": "string"
    }
  },
  "storage": {
    "s3storeadmindashboardstorage3e699dde": {
      "BucketName": "string",
      "Region": "string"
    }
  }
}