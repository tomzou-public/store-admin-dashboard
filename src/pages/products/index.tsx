import ProductCreateForm from "@/ui-components/ProductCreateForm";
import { Flex, Heading } from "@aws-amplify/ui-react";
import router from "next/router";

export default function Products() {
	return (
		<Flex justifyContent="center" direction="column" alignItems="center">
			<Heading level={1}>Create New Product</Heading>
			<ProductCreateForm
				width="70%"
				padding="2rem"
				border="1px solid black"
				borderRadius="1 rem"
				onSuccess={() => router.push("/")}
			/>
		</Flex>
	);
}
