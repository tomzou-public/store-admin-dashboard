import { useEffect, useState } from "react";
import { generateClient } from "aws-amplify/api";
import { GraphQLQuery } from "@aws-amplify/api";
import * as queries from "@/graphql/queries";
import ProductDetails from "@/components/ProductDetails";
import { ListPlatformsQuery } from "@/API";
import { TableValues } from "@/types/types";

export default function Platforms() {
	const [platforms, setPlatforms] = useState<TableValues[]>();

	useEffect(() => {
		async function grabPlatforms() {
			const client = generateClient();
			const allPlatforms = await client.graphql<GraphQLQuery<ListPlatformsQuery>>({
				query: queries.listPlatforms,
			});
			setPlatforms(allPlatforms.data?.listPlatforms?.items as TableValues[]);
		}
		grabPlatforms();
	}, []);

	return <ProductDetails headingName="Platforms" items={platforms} />;
}
