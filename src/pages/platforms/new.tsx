import PlatformCreateForm from "@/ui-components/PlatformCreateForm";
import { Heading, Flex } from "@aws-amplify/ui-react";
import router from "next/router";

export default function PlatformsNew() {
	return (
		<Flex justifyContent="center" direction="column" alignItems="center">
			<Heading level={1}>Create New Platform</Heading>
			<PlatformCreateForm
				width="50%"
				border="1px solid black"
				borderRadius="1 rem"
				onSuccess={() => router.push("/genres")}
			></PlatformCreateForm>
		</Flex>
	);
}
