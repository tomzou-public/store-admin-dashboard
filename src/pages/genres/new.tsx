import GenreCreateForm from "@/ui-components/GenreCreateForm";
import { Flex, Heading } from "@aws-amplify/ui-react";
import router from "next/router";

export default function GenreNew() {
	return (
		<Flex justifyContent="center" direction="column" alignItems="center">
			<Heading level={1}>Create New Genres</Heading>
			<GenreCreateForm
				width="50%"
				border="1px solid black"
				borderRadius="1 rem"
				onSuccess={() => router.push("/genres")}
			></GenreCreateForm>
		</Flex>
	);
}
