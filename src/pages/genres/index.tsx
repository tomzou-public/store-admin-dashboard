import { useEffect, useState } from "react";
import { generateClient } from "aws-amplify/api";
import { GraphQLQuery } from "@aws-amplify/api";
import * as queries from "@/graphql/queries";
import { ListGenresQuery } from "@/API";
import { TableValues } from "@/types/types";
import ProductDetails from "@/components/ProductDetails";

export default function Genres() {
	const [genres, setGenres] = useState<TableValues[]>();

	useEffect(() => {
		async function grabGenres() {
			const client = generateClient();
			const allGenres = await client.graphql<GraphQLQuery<ListGenresQuery>>({
				query: queries.listGenres,
			});
			setGenres(allGenres.data?.listGenres?.items as TableValues[]);
		}
		grabGenres();
	}, []);

	return <ProductDetails headingName="Genres" items={genres} />;
}
