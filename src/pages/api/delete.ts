import type { NextApiRequest, NextApiResponse } from "next";
import { GraphQLQuery, generateClient } from "@aws-amplify/api";
import * as mutations from "@/graphql/mutations";
import { DeleteProductInput } from "@/API";
import { checkIsAdmin } from "@/utils/amplifyServerUtils";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
	if (req.method === "DELETE") {
		const { id } = JSON.parse(req.body);
		try {
			const isAdmin = await checkIsAdmin();
			if (isAdmin) {
				// delete user
				const client = generateClient();
				await client.graphql<GraphQLQuery<DeleteProductInput>>({
					query: mutations.deleteProduct,
					variables: { input: { id } },
				});
				res.status(200).json({ success: true, message: "success" });
			} else {
				// Not in admin group send back authorization failed
				res.status(401).json({ success: false, message: "Authorization failed" });
			}
		} catch (e) {
			// Not logged in
			console.log("error", e);
			res.status(401).json({ success: false, message: "Authentication failed" });
		}
	} else {
		// Not DELETE method
		res.status(401).json({ success: false, message: "Authentication failed" });
	}
}
