import type { AppProps } from "next/app";
import { Amplify } from "aws-amplify";
import { Authenticator } from "@aws-amplify/ui-react";
import amplifyConfig from "@/amplifyconfiguration.json";
import Layout from "@/components/layout";
import "@/styles/globals.css";
import "@aws-amplify/ui-react/styles.css";

Amplify.configure(amplifyConfig, { ssr: true });

export default function App({ Component, pageProps }: AppProps) {
	const formFields = {
		signIn: {
			username: {
				placeholder: "admin@store.com / user2@store.com",
			},
			password: {
				placeholder: "123456qwe",
			},
		},
	};

	return (
		<Authenticator formFields={formFields}>
			<Layout>
				<Component {...pageProps} />
			</Layout>
		</Authenticator>
	);
}
