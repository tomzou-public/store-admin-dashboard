import { Button, Flex, Heading } from "@aws-amplify/ui-react";
import { generateClient, GraphQLQuery } from "aws-amplify/api";
import * as queries from "@/graphql/queries";
import { ListProductsQuery, Product } from "@/API";
import { useState, useEffect } from "react";
import ProductsTable from "@/components/ProductsTable";
import router from "next/router";
import { signOut } from "aws-amplify/auth";
import { checkIsAdmin } from "@/utils/amplifyServerUtils";

export default function Home() {
	const [products, setProducts] = useState<Product[]>();
	const [isAdmin, setIsAdmin] = useState<boolean>(false);

	useEffect(() => {
		async function load() {
			const isAdmin = await checkIsAdmin();
			console.log(isAdmin);
			setIsAdmin(isAdmin);

			const client = generateClient();
			const allProducts = await client.graphql<GraphQLQuery<ListProductsQuery>>({
				query: queries.listProducts,
			});
			setProducts(allProducts.data?.listProducts?.items as Product[]);
		}
		load();
	}, []);

	const onClickDelete = async (id: string) => {
		if (!id) return;
		const res = await fetch("/api/delete", {
			method: "DELETE",
			body: JSON.stringify({ id }),
		});
		if (res.status === 200) {
			const filteredProducts = products?.filter((product) => product.id !== id);
			setProducts(filteredProducts);
		}
	};

	return (
		<>
			<Flex justifyContent="center" alignItems="center" direction="column">
				<Flex
					direction="row"
					justifyContent="space-between"
					alignItems="center"
					gap="1rem"
					width="100%"
					padding="1rem"
					backgroundColor="white"
					className="underline"
				>
					<Heading level={1}>All Products</Heading>
					<Button
						onClick={() => {
							router.push("/products");
						}}
					>
						Add Product
					</Button>
				</Flex>
				{products === undefined || products.length === 0 ? null : (
					<ProductsTable products={products} onClickDelete={onClickDelete} admin={isAdmin} />
				)}
				<Button onClick={async () => await signOut()} variation="primary">
					Sign Out
				</Button>
			</Flex>
		</>
	);
}
