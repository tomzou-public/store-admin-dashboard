import { createServerRunner } from "@aws-amplify/adapter-nextjs";
import config from "@/amplifyconfiguration.json";
import { fetchAuthSession } from "aws-amplify/auth";

export const { runWithAmplifyServerContext } = createServerRunner({
	config,
});

export async function checkIsAdmin() {
	try {
		const groups = (await fetchAuthSession()).tokens?.accessToken?.payload["cognito:groups"] as string[];
		return groups?.includes("admin") ?? false;
	} catch (err) {
		console.log(err);
		return false;
	}
}
