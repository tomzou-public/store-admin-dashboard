"use client";
import React, { useState } from "react";
import Link from "next/link";
import { Button, Divider, Flex } from "@aws-amplify/ui-react";
import clsx from "clsx";
import { usePathname } from "next/navigation";
import { useEffect } from "react";
import { getCurrentUser } from "aws-amplify/auth";

export default function Layout({ children, ...props }: { children: React.ReactNode }) {
	const [name, setName] = useState<string>();
	useEffect(() => {
		const getUser = async () => {
			const { username, userId, signInDetails } = await getCurrentUser();
			setName(username);
		};
		getUser();
	}, []);

	const pathname = usePathname();
	const routes = [
		{
			href: "/",
			label: "Overview",
		},
		{
			href: "/genres",
			label: "Genres",
		},
		{
			href: "/platforms",
			label: "Platforms",
		},
		{
			href: "/products",
			label: "Products",
		},
	];
	return (
		<>
			<Flex direction="row" justifyContent="space-between" alignItems="center" marginBottom="1rem">
				<Flex as="nav" alignItems="center" gap="3rem" margin="0 2rem" {...props}>
					{routes.map((route) => (
						<Link key={route.href} href={route.href}>
							<p
								className={clsx("", {
									"font-bold": pathname === route.href,
								})}
							>
								{route.label}
							</p>
						</Link>
					))}
				</Flex>
				<Button variation="primary" borderRadius="100%">
					{name?.charAt(0)}
				</Button>
			</Flex>
			<Divider size="small"></Divider>
			<div className="p-8">{children}</div>
		</>
	);
}
